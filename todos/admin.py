from django.contrib import admin
from .models import TodoList
from .models import TodoItem


class TodoListAdmin(admin.ModelAdmin):
    list_display = ("name", "id")


class TodoItemAdmin(admin.ModelAdmin):
    list_display = ("task", "due_date")


admin.site.register(TodoList, TodoListAdmin)
admin.site.register(TodoItem, TodoItemAdmin)
